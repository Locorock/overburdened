#Overburned

## Basic Gist

You are a mimic, a backpack mimic, you look like a backpack but you do mimic stuff. A backpack isn't like the most comfortable way to go around, so you "borrow" dead bodies and ~~control them~~ let them carry you around. Basically, you are a guest and they are your host.

### Why tho

Look my friend, i just wanted a reasonable excuse to make a ttrpg about inventory managment where the only thing you have to keep track of is your inventory. Also, because it's funny. 

#### Time

- 6 seconds: Round *Combat and action*
- 6 minutes: Stretch *Actions and local movement*
- 6 hours: Watch *Rest and global movement*
- 6 days: Week *Used mostly for side activities*
- 6 weeks: Month
- 6 months: Year *Used for long accomplishments, also scans the passing of the seasons*

#### Lexicon

- Things that occupy slots: Slices
- Wounds and Stress: Wear and Tear
- Level-Up: Mutate
- Experience: Biomass
- Move 1 item in the inventory: Shift

#### Style Guide (Colors soon)
- **{Stats}**
- (*Actions*)
- [**Tags**]
- *Slices*
- **Special Slices**
- {**2**|**2**}: *Slot Size*

#### Checks and Rolls

Most checks (saving throws, skill checks and so on) rely on the two main stats: Host and Guest. When resolving an action that carries a moderate or higher amount of danger a character should roll for the relevant stat. Stats are represented with positive integers. Whenever a check is required, a player must roll a d6+the relevant stat, on a result higher than 6 they succeed, on a 1 they fail horribly. 

---

## Inventory

#### Core

Starts out composed of a square of 9 slots, and is then modified by character construction and subsequent character improvements.

#### Hands

You start with X hand slots, where X is the number of hands your host possesses. These slots can each hold up to 6 regular slots worth of slices and don't care about the shape of the item, but they are not part of the invenotry, and moving an item from and to them requires a {*Wield*}/{*Drop*} action. 

#### Mutation

At level 1 and every level thereafter the player can roll 3d6 and based on its result on the ““Table”” adds a specific mutation to his core. 
A mutation is a specific improvement to the pc that is bound to it by an add-on that is placed on the rim of the player’s inventory and allows them to obtain extra slots Each mutation has a predefined shape that can be changed depending on how useful the mutation would be in the current campaign. 
If the player doesn’t like what it rolled he can reroll once and then decide which of the two results to keep. Or just let them pick their mutations, your choice. Possible shapes ordered by how good they are:

- \+ shape: 5 squares, kinda unwieldy tho
- T shape: 4 squares, fits almost everywhere
- L shape: 4 squares, fits nicely in a lot of situations
- I shape: 4 square, kinda better than square but not by much
- \[\] shape: 4 squares, doesn’t really fit anywhere nicely
- I shape: 3 squares, when you really shouldn’t be giving this mutation tk anybody
- And so on, be my guest honestly

These add-ons can be rotated freely or mirrored during placement only. Once placed they are permanently locked in place Example mutations: See 3d table ™️

#### Stats

The two main stats, Host and Guest, are used mainly outside of combat to roll for saves, host is also used for healing wear and tear, while guest is also used to determine how many actions the mimic can take in a round. Host represents the mimic’s control over his host, his hardiness and reflexes, while Guest represents the mimic’s ability to think, perceive and communicate. Host and Guest are associated to values that are multiples of 2. Hostd is based on the width of the inventory, while Guest is based on the height of the inventory (example: 5 width and 4 height means 5 Host and 4 Guest)"

#### Instability

Whenever a new add-on is placed instability of the new shape must be determined. It is calculated taking the largest possible rectangle containing the inventory and counting how many cells aren’t part of the inventory in that rectangle. The new shape’s instability is calculated by taking this count and dividing it by 2 (round down). For each point of instability in a shape a player must choose a cell of his inventory and render it unstable. Unstable cells can’t usually be occupied by most items, but items that can occupy unstable slots mist possess the Stable tag. Unstable cells remain unstable until instability is recalculated on placement of a new add-on

#### Pockets

Pockets are temporary add-ons that don’t cause recalculation of instability when placed and don’t change Host and Guest dice values. They are usually placed by spells or potions and allow the player to own extra cells for a limited amount of time. When a pocket disappears all items inside it are moved to the closest available cell (order and ties are decided by player), if that is not possible, items that can be dropped will be dropped, while undroppable items will have to be treated as if they attempted entering the inventory in that moment (Example: a pocket containing a Tear while there are no cells available to move it to will kill you when it disappears)

#### External Inventories

The players are not the only entities that can hold inventories, but most peculiarities of the Host/Guest system are not valid for non-mimic entities. Inventories of NPCs for example work in similar way to player inventories, but they are not related to the npc’s stats and npcs have to use an action to activate items in their backpack, like potions and spells, or to move items inside it. Inanimate objects can be considered instead as pouches, that might or might not be carriable by players and cannot be interacted directly with guest actions, but can also store regular items, an example would a luggage, a donkey’s bags or a cart.

---

## Slices

***Slice***: entity (physical or abstract) that can occupy one or more slots inside an inventory

#### Descriptors

Descriptors define common properties among many slices, and are meant to reduce the amount of text written on each slice

- [**Small**], Small items are items that wouldn’t occupy much space in the backpack and that aren’t particularly heavy, while no item can be stacked and they occupy 1 slot when in the main inventory, they can occupy pouch slots

- [**Bound**], Describes a slice that cannot be [**Shift**]ed or [**Drop**]ped

- [**Abstract**], Abstract slices cannot be [**Wield**]ed

- [**Capacity N**], Describes a slice that can store other slices in an external inventory with N being the capacity of said slice, look at external inventories for more indepth info

- [**Discrete N**], Describes a slice that possesses use charges, whenever this slice is used a charge is removed from it, if no charges remain, the slice is deleted

    - [**Whole**], Describes a [**Discrete**] slice that cannot be [**Split**]

- [**Equip**], Describes a slice that needs to be {
    Bound} before it applies its effect, usually associated with armors and worn slices.

- [**Cumbersome**], Describes a slice that requires +2H to be {
    Bound
}

- [**Stable**], Describes a slice that can occupy unstable slots

- [**Stacked**], Describes a slice that symbolizes an indefinite amount of tools/equipment, usually occupies more slots than a regular version itself, but cannot [Degrade] and cannot be [Bound] or [Wield]ed, except by entities that specify they can do so

#### Special Slices

**Wear / Tear**

Wear is an an abstract concept used to simulate exhertion, it occupies always one slot and is added to the inventory by some actions. Wear is usually removed by resting, a short rest removes Wear equal to HOST, a long rest removes all Wear

Tear is used to indicate a wound on the Guest, it occupies always one slot and is added to the inventory when the PG takes unprevented damage, when more than 1 Tear is added at once, all the placed Tears must be connected. Tears are usually removed by resting, a short rest removes no Tears, a long rest removes Tears equal to HOST

**Condition**

Conditions are slices that are inserted into the inventory and usually have a limited duration, indicated by charges. At the start of every round everyone with a condition in their inventory must resolve it and remove one charge from it. Whe there are no more charges to remove the condition is removed from the inventory

**Spell Material**

Imprecise amounts of a determinate spell material, each charge of this slice is enough for a single **Invoke**. Spell materials might be plant based (like flowers and roots), animal based (like frog legs or stoat liver) or powders of different nature (like copper dust or sugar)

**Pouches**

Invocation Pouches are specialized material pouches that allow the user to **Invoke** spells using the materials they contain, Invocation pouches should be more expensive, occupy more slots and should store less items than regular material pouches

---

## Combat

#### Zones

#### Attacking

**Initiating**

By performing an [Use] action on a [Wield]ed weapon you can initiate an attack on a target. The range at which you can attack a target is defined by the kind of weapon and the specified kind of use (You might want to throw an axe instead of swinging it). When you attack, look at the target's inventory, draw a box around it that can contain it and has both width and height as multiples of 2. Roll then 2 dice, with values equal to the width and height of the drawn box. The resulting coordinate indicate the target slot for the attack. If you hit an area outside the inventory, you miss.

**Resolving an Attack**

Look at the item in the target slot. Depending on the tags of such item, different effects may apply: [Armor] soaks damage you deal up to it's value, while [Spiked] might damage you back. After all effects are resolved, the defender must place X [Wound]s in contiguous slots, including the target slot, where X is the damage of your weapon minus any reduction.

**Placing Wounds**

Placing a **wound** in an inventory means [**Drop**]ping whatever [**Drop**]pable object that occupies that slot and placing a **Wound** in that slot, if the object in question is [**Bound**], it is [**Broken**] and then [**Drop**]ped. If the defender cannot place a Wound in its inventory, that character falls [**Unconscious**]. 

#### Equipment Archetypes

**Weapons**

- Mace: Always deals at least 1 damage
- Spear/Dagger: Ignores 1 armor, but doesn't [**Break**] it on wounds
- Axe: Breaks shields even if no wounds are dealt
- Bow: Ranged, requires Ammo
- Crossbow: Ranged, requires Ammo and reloading (1 host action), same trait as a spear
- Parrying Weapon: Also works as a shield
- Sword: can be two-handed to gain [**Stance**]
- Staff: Gain advantage against Trip Actions
- Whip: Can initiate a grapple (it always works, enemy is bound to you and cannot move away and can only free itself with a successfull host check) from range
- Gun: Ranged, Has to be reloaded, requires ammo, rolls for damage, preferably lots of coins, for flavour

**Shields**

- *Buckler*, 2 block, cheap and fast to repair, cannot block ranged. 2x2 
- *Heater*, 3 block. 2x3. [**Equip**]
- *Tower*, 4 block, only allows 1 attack or 1 move per round. {**2x4**} [**Stance**, **Equip**]
- *Spiked*, 2 block, usually better bash damage 2x3. [**Spiked**, **Equip**]
- *Metal*, 3 block, 2x3 [**Discrete**, **Equip**]

**Armor**

- *Light*, 2 block, 2x2 slice [*Equip*]
- *Medium*, 3 Block, 2x3 Slice [*Equip*, *Cumbersome*]
- *Heavy*, 4 Block, 2x4 Slice [*Equip*, *Cumbersome*]
- *Skulker*, 2 block, 2x3 slice 
- *Ankh*, 3 block, 2x4 slice [*Equip*, *Cumbersome*, *Warded*]

#### Action Economy

Actions economy in combat is dictated by Guest and Host. You have up to **{GUEST}** Guest Actions every round and up to **{BODY}**/2 (rounded down) Body Actions every round.
A Guest action could also be called as a backpack action, as in the mimic does something, either inside of itself or with its tentacles.

**Guest Actions**

- [**Shift**] (1G): Move a slice inside your main inventory
- [**Swap**] (2G): Swap the position of 2 slices in your main inventory
- [**Use**] (1G): As in use an slice, like throw a stone or light a torch, you can only [Use] small slices with a guest action
- [**Inject**] (2G): Activate and consume a potion or another fluid substance 
- [**Drop**] (1G): Remove a **Slice** from the inventory and let it fall (to the ground)
- [**Spark**] (1G): Activate an **Invocation** pouch in your inventory
- [**Split**] (1G): Remove a **Charge** from a [**Discrete**] **Slice** and create an identical **Slice** with 1 **Charge** in any valid **Slot** of your **Inventory**
- [**Merge**] (1G): Delete a [**Discrete**] **Slice** from your inventory and add X **Charge**s to another identical **Slice** in your inventory, where X equals the [**Charge**]s of the first **Slice**
- [**Focus**] (1G): Invoke a [**Spark**]ed and [**Ready**] **Invocation** Pouch

**Host Actions**

- [**Use**] (1H): As in use a slice, like swing a sword or shoot a bow, using slices with Weight 6 or higher requires one extra HOST action 
- [**Move**] (1H): As in you move
- [**Duck**] (1H): Drop to the ground, [Move] costs 1H more in this state, but you can't be hit by ranged attacks unless they crit.
- [**Stand**] (1H): Stand up, reverting the [Duck] action
- [**Chug**] (1H): Drink a potion or another fluid substance
- [**Bind**] (2H): Bind a slice to your inventory, until this action is repeated on it, that slice gains [**Bound**]
- [**Trip**] (1H): Try to trip an opponent, forcing him to [**Duck**] if he fails a Host check
- [**Grapple**] (1H): See whip

**Action Penalties**

Your DM might apply extra costs to your actions in case a circumstance makes performing that action harder. Like for example moving through a difficult terrain like a swamp might require you to spend 2H for a [Move].

#### Multitudes

Big groups of identical or similar enemies might be assomilated in a single entity called a Multitude. Multitude are built by taking the inventory of each individual and stiching them toghether. Stats are then calculated from the new inventory and the entity is considered for mechanichal purposes a single enemy. To simulate the loss of numbers upon taking wounds, instead of placing wounds in the inventory of a Multitude, delete the slot in which a wound would be placed. If the inventory would be split into multiple non-contiguous pieces, the multitude in split into that many pieces. Multitudes should use [Stacked] weapons and armors, to avoid the problems that arise from multiple weapons on different entities. 

#### Initiative

When an encounter starts, all sides roll a d6 as a group, the initiative order is sorted with the highest rollers going first, if one side doesn't notice another side, they automatically lose their position to them. Internally each side can decide how to order their characters. 

---

## Invocations

#### Invocation Pouches

Magic in this world is bound by the alchemical reactions of particular substances inside apposite "pouches" that allow precise manipulation of such reagents. Trying to **Invoke** without an **Invocation** Pouch is possible but requires time and precision to arrange the components correctly, something not available during combat.

#### Notes

- **Pattern** based
- **Pattern**s discovered from trial and error, 1d20 for every combination, it works on 20, it works really wrong on 1, effect potency is based on pattern complexity and material quality
- Invocation Pouch to actually **Invoke**, positioning of ingredients matters, can also **Invoke** without a pouch, but it takes a **Stretch** to position ingredients correctly
- Each correct Invocation is linked to a determinate element, be it abstract or not, Depending on the complexity of the Pattern, the Invocation can be: **Minor**, **Major** or **Foolish**
- [**Spark**]ing a pouch starts the **Invocation** and consumes ingredients, the time and conditions it takes for the spell to be ready to be focused depend on the type of **Invocation** Pouch
- [**Focus**]ing a spell means giving it shape and purpose, a spell might invoke "Flame", but only focusing can turn it into a wall of fire or a firebolt. Spells can be **Invoke**d without **Focus**ing, but in that case the element invoked is spread around the **Invoke**r in a raw, uncontrollable form. This might be accettable in case of "Air" or "Water", but might burn you alive in case of "Fire"
- The kind of [**Focus**] used is determined by the invocation pattern: Different
  reagents added allow the [**Focus**] to change accordingly, these can be
  classified roughly in 5 categories:
    - **Astringents**, that change and restrict the shape and area affected by an invocation
    - **Catalysts**, that enhance determinate characteristics of an invocation
    - **Thaumifiers**, that refine the invocation to allow it to better ignore nature's laws in determinate conditions
    - **Elixirs**, that dramatically change the invocation's function
    - **Binding Agents**, that link together two different invocations
- Different kinds of Invocation pouches:
  - **Channel**, once sparked it takes a full round for the Invocation to focusable, but the invocation remains focusable for a Stretch
  - **Burst**, once sparked the spell is immediately focusable, but Invocating generates Flux *(Extremely reactive gas, can occupy [Unstable] slots, [Bind]s itself when entering the inventory)*
  - **Echo**, once sparked it takes a full round for the Invocation to be focusable, the pouch can be sparked again with no Action Cost.
  - **Twin**, once sparked it takes three full rounds for the invocation to be focusable, but when focused the spell is invoked twice. 
  - **Chain**, once sparked the spell can be immediately focused if the Invoker by Sparks again the pouch
  - **Ravenous**, once sparked the invocation must be immediately focused, each round thereafter in which you do not invoke a spell the pouch gains [**Primed**] *(roll a d6 every start of turn, if you roll 1 the pouch detonates, if you roll 6 the pouch loses [Primed], spreading flux gas in the nearby area and dealing 3 direct wounds to the wearer, if any)*

#### Extra

Different pouches mostly allow to change or skip the focusing modes and times, not the actual focusing in act. The kind of focusing is determined by the base invocation and the catalysts, astringents and reagents added on top of the base formula

---

## Biomass

Biomass is produced by consuming flesh and bones, the amount of biomass produced depends on the host and guest stats of the being consumed. The idea is that biomass is produced by consuming flesh, bones, memories and knowledge. 

#### Gaining Biomass

**Consuming Enemies**

Consume a dead enemy, by digesting it after having ingested its body. Digestion takes a short rest to complete and you can only digest 1 enemy at a time. At completion you gain Bm equal to its Host+Guest. You also gain part of its memories, those near to its death, and those bound by strong emotions. 

**Discoveries**

By discovering an important fact, a secret or a peculiar place you can gain Biomass

**Brain in a Jars**

Some kind of lich left these brain filled jars around in all kind of weird places, it is said that they store pieces of his memory. Consuming one of these gives you 50 Biomass and a random <Revelation>

## Mutating

You can mutate by spending biomass equal to your amount of mutations * 100. Mutating takes a full rest where you are completely unable to act.

---

## Discovery

#### Ouros

The continent Ouros, a giant depression in the middle of the ocean, shielded from the waves by a titanic circular formation of mountains, the Spine. Between these mountains is what remains of the floor of an ancient sea, with coral formations sprouting everywhere, deep trenches, newformed swamps, active volcanoes, salty lakes and rivers. The Spine opens up in only one passage, the Mouth, where the sea falls violently into the lowlands, and forms a myriad of rivers. At the center of the depression lies an enormous sinkhole, made of spiraling mineral formations, the Gate. Long tunnels in the Spine and been dug back during the first days of Birth, these tunnels allowed travelers to come and go from Ouros without braving the Mouth's violent waters, but decades of isolationism have led these tunnels to fall into disrepair. Few remember the cause of the current political situation in Ouros, and even fewer are willing to talk about it. What is known is that passage in or out of this continent is forbidden in most countries.You were board deep within lake Nereo, in a colony of Mimus Orosia, mimics that is.

#### Random Notes

The gate is bottomless and keeps expanding, it is the source of all magic and is actually a gate to another reality opened by Ouroboros to fuel its existence. The high concentrations of flux gas that it generate make it impossible to brave its depths without proper gear, also yeah slap in some made in abyss layers because why not. Ouroboros keeps on unfolding and is said to wake up in the next millenia. Cult of Boros wants to see it unfold and ravage the planet. The tunnels in the spine were sealed centuries ago, since the deep mining within them awakened a sliver of Ouroboros mind, spawning horros from the Other that flooded the entirety of the north sector. Since then access to any of these tunnels is prohibited to common folk, so as to avoid any additional happy little accidents.  
